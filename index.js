const pdfRenderer = require('@agilecards/pdf-renderer/renderer');
const printer = require('printer');
const gm = require('gm');
const fs = require('fs');
const os = require('os');
const path = require('path');
const exec = require('child_process').exec;

const adobePath = 'C:\\Program Files (x86)\\Adobe\\Acrobat Reader DC\\Reader\\AcroRd32.exe';

const print = (issues, options) => {
    return new Promise((resolve, reject) => {
        pdfRenderer.render({
            url: options.rendererUrl,
            options: options.pdfOptions,
            postData: {
                issues
            }
        }).then(pdfData => {
            if (process.platform === 'win32') {
                const tmpFile = path.resolve(os.tmpdir(), 'agile-cards.pdf');
                fs.writeFile(tmpFile, pdfData, err => {
                    if (err) {
                        throw err;
                    }
                    const cmd = `"${adobePath}" /h /n /s /t "${tmpFile}"`;
                    console.log(`executing: ${cmd}`);
                    exec(cmd);
                });
            } else {
                printer.printDirect({
                    printer: options.printer.name,
                    data: pdfData,
                    type: 'PDF',
                    options: {
                        media: options.printer.pageSize
                    },
                    success: resolve,
                    error: reject
                });
            }
        }).catch(reject);
    });
};

const getPrinterNames = () => printer.getPrinters().map(printer => printer.name);

module.exports = {
    print,
    getPrinterNames
};
